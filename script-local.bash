#!/bin/bash
# Script de lancement en local sur un nouvel ordi

#secrets docker
echo "-- CRÉATION DES SECRETS DOCKER --"

mkdir -p secrets
cd secrets
touch user_pg.pw db_pg.pw pwd_pg.pw

echo "saisir un nom d'utilisateur"
read UTILISATEUR
echo $UTILISATEUR > user_pg.pw
unset UTILISATEUR

echo "saisir un nom de base"
read BASE
echo $BASE > db_pg.pw
unset BASE

echo "saisir un mot de passe"
read MOTDEPASSE
echo $MOTDEPASSE > pwd_pg.pw
unset MOTDEPASSE

echo "-- SECRETS DOCKER CRÉÉS --"

#docker-compose.override.yml
echo "-- CRÉATION DU OVERRIDE --"

cd ..
touch docker-compose.override.yml
echo "

networks:
  traefik:
    external: false
  site_imladris_reseau:
    external: false

volumes:
  imladris_rdf_data:
    external: false
  imladris_postgres_data:
    external: false
  imladris_redis_data:

services:
  site-imladris:
    ports:
      - 8080:5000
    environment:
      IMLADRIS_DEBUG: 'True'
      IMLADRIS_THREADS: 2
      FLASK_ENV: development
" > docker-compose.override.yml

echo "-- OVERRIDE CRÉÉ --"

echo "fin"
