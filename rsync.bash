#!/bin/bash
# Script de synchronisation de fichier avec le serveur.
# Variables d'environnement nécessaires :
# 	SSH_CLEF : clef SSH d'accès au serveur
#		SSH_PORT : port d'accès au serveur
#		DESTINATION : destination sur le serveur.

# ------------ Variables (test et initialisation) ------------ #

if [ -z "$SSH_CLEF" ]
then
  echo "Variable SSH_CLEF non définie"
  exit 1
fi
if [ -z "$SSH_PORT" ]
then
  echo "Variable SSH_PORT non définie"
  exit 1
fi
if [ -z "$DESTINATION" ]
then
  echo "Variable DESTINATION non définie"
  exit 1
fi

CONNECTION="ssh -i $SSH_CLEF -p $SSH_PORT"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# ------------ script ------------ #

SOURCE_0="$SCRIPT_DIR/deploy.bash"
SOURCE_1="$SCRIPT_DIR/app/src/vitrine/images"
SOURCE_2="$SCRIPT_DIR/app/src/vitrine/download"
SOURCE_3="$SCRIPT_DIR/app/src/vitrine/static/webfonts"

echo synchronisation des fichiers ...
rsync -avzc -e "$CONNECTION" $SOURCE_0 $SOURCE_1 $SOURCE_2 $SOURCE_3 $DESTINATION
echo fait
