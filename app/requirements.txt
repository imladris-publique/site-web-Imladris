async-timeout==4.0.2
click==8.1.3
Flask==2.2.3
Flask-Assets==2.0
itsdangerous==2.1.2
Jinja2==3.1.2
MarkupSafe==2.1.2
redis==4.5.4
waitress==2.1.2
webassets==2.0
Werkzeug==2.2.3
