
## Liens utiles :

- blueprint :
  - https://realpython.com/flask-blueprint/#how-to-use-flask-blueprints-to-improve-code-reuse
  - https://exploreflask.com/en/latest/blueprints.html
  - https://medium.com/innovation-res/flask-blueprints-tutorial-74862e5a19fd
  - En particulier pour les Bundle d'assets :  
    http://hackersandslackers.io/flask-blueprints/
  - Config sérieuse de Flask :  
    https://flask.palletsprojects.com/en/2.1.x/config/
- jinja :
  - https://jinja.palletsprojects.com/en/3.1.x/templates/
  - https://jinja.palletsprojects.com/en/3.0.x/templates/#macros
- BDD (à plat, SQL, sqlalchemy ... )
- https://www.membershipsthatpay.com/gestion-des-donnees-avec-python-sqlite-et-sqlalchemy-real-python/
- Serveur de prod waitress
  - https://flask.palletsprojects.com/en/2.1.x/tutorial/deploy/
  - https://docs.pylonsproject.org/projects/waitress/en/stable/arguments.html

## Arborescence

- flask.Dockerfile  
  Permet de générer une image docker à partir des sources de l'application.
- requirements.txt  
  Les dépendances python nécessaires pour l'application.
- run.py  
  Programme général, lancé au lancement de l'application.  
  C'est un fichier minimal, permettant de charger les fichiers permettant l'initialisation de l'application et de la lancer.
- **src/**  
  Contient les sources de l'application.
  - `__init__.py`  
    Initialise l'application et les différents modules.
  - dao.py  
    Défini les différents DAO utilisables dans l'application.
  - **actionnaire/**
    Module servant à l'administration des actions, à leur échange, leur transformation etc...  
    Ce module sert également à la gestion des comptes d'associés.
  - **ag/**
    Module servant au déroulement et à la mise en place des assemblées générales d'Imladris.  
    Ce module intègre également la gestion des procès verbaux.
  - **annuaire/**
    Module présentant les différentes organisations à Lanvollon (publiques, associatives ou entreprises), qu'elle fasse ou non partie de la Coopérative.
  - **calendrier/**  
    Module décrivant les différents évènements à Lanvollon. Indépendement ou non des activités propres à Imladris.
  - **equipe/**
    Module concernant les équipes coopératives. Leurs animations, activités, financement...  
    Indépendamment du module vitrine (générique) ou du calendrier général.
  - **groupe/**
    Module de la plateforme propre aux groupes et aux oeuvres.
  - **identite/**  
    Module de gestion des identités d'une personne ou d'un groupe
  - **login/**  
    Module d'inscription des personnes et d'identification des identités.
  - **partage/**
    Module de partage de connaissances, de problèmes, de savoir faire.
  - **vitrine/**  
    Module vitrine  
    Il s'agit du site vitrine d'Imladris, permettant la présentation de l'activité.  
