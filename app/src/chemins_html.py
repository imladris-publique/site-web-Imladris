#! /bin/python3
#-*- coding: utf-8 -*-

from flask import render_template, request
#from controls import get_season_anime

from . import app

@app.route('/', methods=['GET'])
def index():
    """
    root page view that returns the index page and its data
    :return: index template
    """
    # season_anime = get_season_anime()
    return render_template('home.html')
