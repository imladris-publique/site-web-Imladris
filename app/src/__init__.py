#! /bin/python3
#-*- coding: utf-8 -*-

# code/__init__.py

# from decouple import config
from flask import Flask
import redis

from src.environnement import arguments
from src.vitrine.controlleur import module as module_vitrine

doc = """
Installe le serveur sur le port indiqué en paramètre -p ou --port (defaut 5000)
Utilise une base redis accédée:
- sur à l'ip indiqué en --host (défaut 127.0.0.1)
- sur le port indiqué en paramètre -r ou --redis (defaut 6379)
"""

# -------------------- application ---------------------- #

app 		= Flask(__name__)
#TODO app.secret_key = 'dsdfdsf' // Permet à Flask d'utiliser des cookies signé, en particulier pour usage avec les sessions.
# Se renseigner sur l'extension flask_jwt_extended // https://www.datasciencelearner.com/flask-token-based-authentication-secure-api/

## Modules ( Blueprint )
app.register_blueprint(module_vitrine, url_prefix='/')


# -------------------- base redis ---------------------- #

r = redis.Redis(
    host=arguments.redis_host,
    port=arguments.redis_port
)
