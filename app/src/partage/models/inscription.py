#! /bin/python3
#-*- coding: utf-8 -*-

# app/models.py
from dataclasses import dataclass

@dataclass
class Inscription:
    """
    Modéle pour l'inscription d'une identité à une oeuvre
    """
    
    mal_id: int
    url: str
    title: str
    image_url: str
    synopsis: str
    type: str
    airing_start: str
    episodes: int
    members: int