#! /bin/python3
#-*- coding: utf-8 -*-

# app/models.py
from dataclasses import dataclass

@dataclass
class OEuvre:
    """
    Modéle pour une oeuvre
    """
    
    mal_id: int
    url: str
    title: str
    image_url: str
    synopsis: str
    type: str
    airing_start: str
    episodes: int
    members: int