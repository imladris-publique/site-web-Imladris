#! /bin/python3.10
#-*- coding: utf-8 -*-

# app/models.py
from dataclasses import dataclass, field
from datetime import date

@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=True)
class Personne:
    """
    Modéle pour Personne
    """
    
    _id			:int	= None
    _version	:str	= None
    _nom		:str	= None
    _prénom		:str	= None
    _naissance	:date	= None


# -------------------------------------------------------------------------------------------------------------------- #
# 													Propriétés														   #
# -------------------------------------------------------------------------------------------------------------------- #


### Propriétés d'instance

    # ================ Accesseurs ================ #
	
	@property
	def id(self):		return self._id
	
	@property
	def version(self):	return self._version

	@property	
	def nom(self):		return self._nom
	
	@nom.setter
	def set_nom(self,nom): self._nom = nom
	
	@property
	def prénom(self):	return self._prénom
	
	@prénom
	def set_prénom(self,prénom): self._prénom = prénom
	
	@property
	def age(self):
		t = date.today()
		n = self._naissance
		return t.year - n.year - int( (t.month, t.day) < (n.month, n.day) )

	
# -------------------------------------------------------------------------------------------------------------------- #
# 													Méthodes														   #
# -------------------------------------------------------------------------------------------------------------------- #


	def update(self, data) -> Personne, tuple(Erreur) :
		"""
		Met à jour l'objet avec les données.
		  - **objet** : l'objet modifié.
		  - **erreurs** : les erreurs constatés lors de l'édition.
			Teste les données et la possibilité d'édition à partir des données de l'objet lui-même.
		"""
		raise NotImplemented

	def linearise(self, methode:str='json', link=False)-> str :
		"""
		Renvois une linéarisation de l'objet
			**methode** : la méthode de linéarisation choisie.
			**link**	: inclue les liens avec les autres objets (peut fonctionner avec un int pour des niveaux de lien)
		"""
		raise NotImplemented
	
	def _neutralise(self) -> self:
		""" Méthode interne de neutralisation de donnée, utilisée par self.neutralise() pour l'objet """
		raise NotImplemented

	def _clone(self, into):
		raise NotImplemented
	
	def clone(self):
		raise NotImplemented


	# -------------------- Raccourcis DAO -------------------- #


	def svg(self,data) -> Personne :
		return self.DAO.svg(self,data)
	
	def test_update(self, data) -> Personne, tuple[Erreur] :
		return self.DAO.test_update(self,data)

	def neutralise(self, niveau:int=2) -> str :
		if niveau > 0 :
			return self.DAO.neutralise(self, niveau)
		else :
			return self.clone()._neutralise()




# -------------------------------------------------------------------------------------------------------------------- #
# 												Méthode de classe													   #
# -------------------------------------------------------------------------------------------------------------------- #
	

	@classmethod
	def format_id(cls, id_utilisateur:str) -> ID:
		"""
		Formate l'id pour la classe.
		Renvois un id formaté
		"""
		cls.ID
		raise NotImplemented
	
	@classmethod
	def format_data(cls, data, edit=true) :
		"""
		Teste et formatte les données envoyées par l'utilisateur. Test global, sans accès aux données de l'objet lui même.
		Renvoit les données formatés
		edit : indique s'il s'agit d'une édition de donnée, ou d'une création. (les données nécessaires peuvent être différentes)	
		"""
		raise NotImplemented

	# ----------------- Raccourcis DAO ----------------- #

	@classmethod
	def get_all_id(cls) -> tuple[str]:
		"""
		Classe.**get_all_id**() -> tuple(id_utilisateur)
		convertit le retour du DAO en id_utilisateur

		"""
		
		return ( id.to_user() for id in cls.DAO.get_all_id() )
	

# -------------------------------------------------------------------------------------------------------------------- #
# 													ID																   #
# -------------------------------------------------------------------------------------------------------------------- #

	class ID:
		""" Objet identifiant """
		_id : int=0
		
		def to_user(self):
			raise NotImplemented
			
		def to_bdd(self):
			raise NotImplemented
		
		@classmethod
		def from_bdd(cls, id_bdd:int):
			return cls(id_bdd)
		
		@classmethod
		def from_user(cls, id_user:str):
			raise NotImplemented
			return cls(id_user)
		

# -------------------------------------------------------------------------------------------------------------------- #
# 													Erreur															   #
# -------------------------------------------------------------------------------------------------------------------- #

    class Erreur:
    	pass

# -------------------------------------------------------------------------------------------------------------------- #
# 													DAO																   #
# -------------------------------------------------------------------------------------------------------------------- #
    
    class DAO:
    
		@staticmethod
		def add_new(data) -> Personne :
			"""
			Crée un nouvel objet à l'aide des données
			renvois l'objet créé
			"""
			raise NotImplemented


		@staticmethod
		def delete(id_objet:ID) -> None :
			"""
			Efface l'objet en base.
			"""
			raise NotImplemented

		
		@staticmethod
		def svg(objet, data) -> Personne :
			"""
			Enregistre les modifications en base.
			data est également transmis car il peut contenir des informations annexes à l'objet.
			"""
			raise NotImplemented


		@staticmethod
		def get_from_id(id_objet:ID, exact=false, link=link) -> Personne :
			"""
			Récupère l'objet en base. Renvois une instance de l'objet.
				exact : Renvois None si innexistant, ou envoit une exception si exact=true
		 		link : Niveau de lien récupérés dans l'objet.
					False : aucun
					True : ids des liens directs
					niveau : (entier,champs de booléen,...) récupère également les objets liés ou niveau de liens récupéré
			"""
			raise NotImplemented

		
		@staticmethod
		def test_update(objet:Personne, data) -> Personne, tuple[Erreur]:
			"""
			Test si la modification de l'objet est permise vis à vis de l'ensemble de la base.
			"""
			raise NotImplemented
		

		@staticmethod
		def format_id(id_bdd:int) -> ID:
			"""
			Formate un id de base de donnée en id de classe
			"""
			ID
			raise NotImplemented


		@staticmethod
		def neutralise(id_objet:ID, niveau:int=2) -> Personne:
			"""
			niveau 2 : annonymise l'objet en base
			niveau 1 : annonymise et délie l'objet en base
			niveau 0 : neutralise l'objet (le remplace par un objet vide)
			"""
			raise NotImplemented
		
		@staticmethod
		def get_all_id() -> tuple[ID]:
			"""
			Renvois l'ensemble des ids utilisées
			"""
			raise NotImplemented



# -------------------------------------------------------------------------------------------------------------------- #
# 													Magiques														   #
# -------------------------------------------------------------------------------------------------------------------- #

