#! /bin/python3.10
#-*- coding: utf-8 -*-

# app/models.py
from dataclasses import dataclass

@dataclass
class Demande:
    """
    Modéle pour une Demande
    """
    
    mal_id: int
    url: str
    title: str
    image_url: str
    synopsis: str
    type: str
    airing_start: str
    episodes: int
    members: int