#! /bin/python3.10
#-*- coding: utf-8 -*-

# models/document.py

from dataclasses import dataclass, field
from datetime import date

@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=True)
class Document:
    """
    Modèle pour un objet Document.
    - id            (int)   : identifiant métier du document
    - uri           (str)   : uri du document (rdf)
    - version       (str)   : version
    - hash          (str)   : hash de contrôle du contenu (indépendament des infos ci-présente)
    - format        (str)   : type de document
    - titre         (str)   : titre
    - description   (str)   : description
    - icone         (Image) : icone ou miniature du fichier
    - chemin        (str)   : chemin du fichier 'stockage'
    - dates         (date,date,date): date de création / dernière modification / dernier accès
    """

    _id			:int	= None
    _uri        :str    = None
    _version	:str	= None
    _hash       :str    = None
    _format     :str    = None
    _titre      :str    = None
    _description:str    = None
    _icone      :str    = None
    _chemin     :str    = None
    _dates      :tuple[date] = (None,None,None)


# -------------------------------------------------------------------------------------------------------------------- #
# 													Propriétés														   #
# -------------------------------------------------------------------------------------------------------------------- #


### Propriétés d'instance

    # ================ Accesseurs ================ #

	@property
	def id(self):      return self._id

	@property
	def uri(self):     return self._uri

	@property
	def version(self): return self._version

	@property
	def hash(self):    return self._hash

	@property
	def format(self):  return self._format

	@property
	def titre(self):   return self._titre

	@property
	def description(self): return self._description

	@property
	def icone(self): return self._icone

	@property
	def chemin(self): return self._chemin

	@property
	def création(self): return self._dates[0]

	@property
	def modification(self): return self._dates[1]

	@property
	def accès(self): return self._dates[2]


# -------------------------------------------------------------------------------------------------------------------- #
# 													Méthodes														   #
# -------------------------------------------------------------------------------------------------------------------- #


	def update(self, data) -> Document, tuple(Erreur) :
		"""
		Met à jour l'objet avec les données.
		  - **objet** : l'objet modifié.
		  - **erreurs** : les erreurs constatés lors de l'édition.
			Teste les données et la possibilité d'édition à partir des données de l'objet lui-même.
		"""
		raise NotImplemented

	def linearise(self, methode:str='json', link=False)-> str :
		"""
		Renvois une linéarisation de l'objet
			**methode** : la méthode de linéarisation choisie.
			**link**	: inclue les liens avec les autres objets (peut fonctionner avec un int pour des niveaux de lien)
		"""
		raise NotImplemented

	def _neutralise(self) -> self:
		""" Méthode interne de neutralisation de donnée, utilisée par self.neutralise() pour l'objet """
		raise NotImplemented

	def _clone(self, into):
		raise NotImplemented

	def clone(self):
		raise NotImplemented


	# -------------------- Raccourcis DAO -------------------- #


	def svg(self,data) -> Document :
		return self.DAO.svg(self,data)

	def test_update(self, data) -> Document, tuple[Erreur] :
		return self.DAO.test_update(self,data)

	def neutralise(self, niveau:int=2) -> str :
		if niveau > 0 :
			return self.DAO.neutralise(self, niveau)
		else :
			return self.clone()._neutralise()




# -------------------------------------------------------------------------------------------------------------------- #
# 												Méthode de classe													   #
# -------------------------------------------------------------------------------------------------------------------- #


	@classmethod
	def format_id(cls, id_utilisateur:str) -> ID:
		"""
		Formate l'id pour la classe.
		Renvois un id formaté
		"""
		cls.ID
		raise NotImplemented

	@classmethod
	def format_data(cls, data, edit=true) :
		"""
		Teste et formatte les données envoyées par l'utilisateur. Test global, sans accès aux données de l'objet lui même.
		Renvoit les données formatés
		edit : indique s'il s'agit d'une édition de donnée, ou d'une création. (les données nécessaires peuvent être différentes)
		"""
		raise NotImplemented

	# ----------------- Raccourcis DAO ----------------- #

	@classmethod
	def get_all_id(cls) -> tuple[str]:
		"""
		Classe.**get_all_id**() -> tuple(id_utilisateur)
		convertit le retour du DAO en id_utilisateur
		"""

		return ( id.to_user() for id in cls.DAO.get_all_id() )


# -------------------------------------------------------------------------------------------------------------------- #
# 													ID																   #
# -------------------------------------------------------------------------------------------------------------------- #

	class ID:
		""" Objet identifiant """
		_id : int=0

		def to_user(self):
			raise NotImplemented

		def to_bdd(self):
			raise NotImplemented

		@classmethod
		def from_bdd(cls, id_bdd:int):
			return cls(id_bdd)

		@classmethod
		def from_user(cls, id_user:str):
			raise NotImplemented
			return cls(id_user)


# -------------------------------------------------------------------------------------------------------------------- #
# 													Erreur															   #
# -------------------------------------------------------------------------------------------------------------------- #

    class Erreur:
    	pass

# -------------------------------------------------------------------------------------------------------------------- #
# 													DAO																   #
# -------------------------------------------------------------------------------------------------------------------- #

    class DAO:

		@staticmethod
		def add_new(data) -> Document :
			"""
			Crée un nouvel objet à l'aide des données
			renvois l'objet créé
			"""
			raise NotImplemented


		@staticmethod
		def delete(id_objet:ID) -> None :
			"""
			Efface l'objet en base.
			"""
			raise NotImplemented


		@staticmethod
		def svg(objet, data) -> Document :
			"""
			Enregistre les modifications en base.
			data est également transmis car il peut contenir des informations annexes à l'objet.
			"""
			raise NotImplemented


		@staticmethod
		def get_from_id(id_objet:ID, exact=false, link=link) -> Document :
			"""
			Récupère l'objet en base. Renvois une instance de l'objet.
				exact : Renvois None si innexistant, ou envoit une exception si exact=true
		 		link : Niveau de lien récupérés dans l'objet.
					False : aucun
					True : ids des liens directs
					niveau : (entier,champs de booléen,...) récupère également les objets liés ou niveau de liens récupéré
			"""
			raise NotImplemented


		@staticmethod
		def test_update(objet:Document, data) -> Document, tuple[Erreur]:
			"""
			Test si la modification de l'objet est permise vis à vis de l'ensemble de la base.
			"""
			raise NotImplemented


		@staticmethod
		def format_id(id_bdd:int) -> ID:
			"""
			Formate un id de base de donnée en id de classe
			"""
			ID
			raise NotImplemented


		@staticmethod
		def neutralise(id_objet:ID, niveau:int=2) -> Document:
			"""
			niveau 2 : annonymise l'objet en base
			niveau 1 : annonymise et délie l'objet en base
			niveau 0 : neutralise l'objet (le remplace par un objet vide)
			"""
			raise NotImplemented

		@staticmethod
		def get_all_id() -> tuple[ID]:
			"""
			Renvois l'ensemble des ids utilisées
			"""
			raise NotImplemented



# -------------------------------------------------------------------------------------------------------------------- #
# 													Magiques														   #
# -------------------------------------------------------------------------------------------------------------------- #



@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=True)
class Image(Document):
    """
    Classe de document spécifique pour les images
    se renvoit lui-même en icone
    """

	@property
	def icone(self): return self
