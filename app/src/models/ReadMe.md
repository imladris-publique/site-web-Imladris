Modèles
=======


Chaque objet métier correspond à un modèle


Opérations de base
------------------

Chaque objet métier dispose d'opération de bases, statiques ou dynamiques qui permettent de le récupérer en base, de l'éditer, de formater les formulaires
Il dispose également d'un DAO pour gérer les opérations en base.

Note sur les Tests : Il y a 3 niveau de test.
1. (Classe.**format_data**)	Le premier niveau teste (éventuellement formate) les données elles-mêmes, vis à vis du type d'objet (sa Classe)
2. (objet.**update**) 		le deuxième niveau teste l'édition vis à vis des données actuelle de l'objet, lui-même
3. (DAO.**test_update**)	le troisième niveau teste une mise à jour vis à vis de l'ensemble de la base. (cohérence des données)



### Méthode dynamiques

- 	objet.**version**
	Renvois un hash du contenu (incluant un numéro de version), qui permetra d'éliminer les risques de conflit de version lors d'édition.
- 	objet.**update**(data) -> objet, erreurs
	met à jour l'objet avec les données.
  - **objet** : l'objet modifié.
  - **erreurs** : les erreurs constatés lors de l'édition.
	Teste les données et la possibilité d'édition à partir des données de l'objet lui-même.
-	objet.**linearise**(methode='json',link=false)
	Renvois une linéarisation de l'objet
	**methode** : la méthode de linéarisation choisie.
	**link**	: inclue les liens avec les autres objets (peut fonctionner avec un int pour des niveaux de lien)

- 	Raccourcis DAO : (cf DAO)
	-	objet.**svg**(data) -> DAO.svg(objet,data)
	-	objet.**test_update**(data) -> DAO.test_update(objet,data)
	- 	objet.**neutralise**(niveau=2) -> DAO.neutralise(self.id, niveau)


### Méthodes statiques (de classe)

- 	Classe.**format_id**(id_utilisateur) -> Id
	Formate un id utilisateur en id de classe
- 	Classe.**format_data**(data,edit=true) -> data
	Teste et formatte les données envoyées par l'utilisateur. Test global, sans accès aux données de l'objet lui même.
	Renvoit les données formatés
	edit : indique s'il s'agit d'une édition de donnée, ou d'une création. (les données nécessaires peuvent être différentes)

-	Raccourcis DAO : (cf DAO)
	-	Classe.**get_all_id**() -> tuple(id_utilisateur)
		convertit le retour du DAO en id_utilisateur


### Méthode du DAO (accès à la base)


- 	DAO.**add_new**(data) -> objet
	Crée un nouvel objet à l'aide des données
	renvois l'objet créé
-	DAO.**delete**(id_objet) -> None
	Efface l'objet en base.
-	DAO.**svg**(objet,data) -> object
	Enregistre les modifications en base.
	data est également transmis car il contient des informations annexes à l'objet. En particulier la version initiale de l'objet (data.version) qui doit correspondre à celle actuellement en base. ( L'édition sera effectuée par une transaction qui vérifiera la correspondance avant modification ).

- 	DAO.**get_from_id**(id_objet, exact=false, link=link) -> objet
	Récupère l'objet en base. Renvois une instance de l'objet.
  -	**exact** : Renvois None si innexistant, ou envoit une exception si exact=true
  -	**link** : Niveau de lien récupérés dans l'objet.
    - False : aucun
    - True : ids des liens directs
    - niveau : (entier,champs de booléen,...) récupère également les objets liés ou niveau de liens récupéré

-	DAO.**test_update**(objet, data) -> object, tuple[Erreur]
	Test si la modification de l'objet est permise vis à vis de l'ensemble de la base.

- 	DAO.**format_id**(id_bdd) -> Id
	Formate un id de base de donnée en id de classe

- 	DAO.**neutralise**(id_objet, niveau=2)
	niveau 2 : annonymise l'objet en base
	niveau 1 : annonymise et délie l'objet en base
	niveau 0 : neutralise l'objet (le remplace par un objet vide)

-	DAO.**get_all_id**() -> tuple[ID]
	Renvois l'ensemble des ids utilisées



Objets Métiers
--------------


### Besoin


### Contact


### Demande


### Identité


### Inscription


### Matériel


### Œuvre


### Personne


### Problème


### Savoir Faire
