#! /bin/python3.10
#-*- coding: utf-8 -*-

from flask import request
from . import app, arguments


doc="""
Pour chacun des modèles :

/api/{model}		GET 	-> récupère la liste des id disponibles en base
					POST	-> ajoute un objet à la base
/api/{model}/{id}	GET		-> récupère les données de l'objet
					PUT		-> modifie l'objet
					DELETE	-> efface l'objet


GET  	: Récupère la valeur de l'objet en json
POST 	: Ajoute un objet en base
PUT  	: Modifie un objet en base (nécessite le n° de version de l'état précédent)
DELETE	: Efface un objet en base
"""



@app.route('/api/<Objet>/<id>', methods=['GET', 'PUT', 'DELETE'])
def api_une_personne(Objet,id):
	"""
	Actions sur une personne déterminée
		GET 	-> retourne les propriétés de la personnes demandée
		PUT 	-> modifie la personne et renvois les nouvelles propriétés
		DELETE 	-> efface la personne de la base
	"""

	link = True # Niveau de lien renvoyé

	# Récupération de la classe de l'objet
	match Objet :
		case 'besoin'		: from models.besoin 		import Besoin		as Objet
		case 'contact' 		: from models.contact 		import Contact		as Objet
		case 'demande' 		: from models.demande 		import Demande		as Objet
		case 'identite'		: from models.identite 		import Identite		as Objet
		case 'materiel'		: from models.materiel 		import Materiel		as Objet
		case 'oeuvre'		: from models.oeuvre 		import OEuvre		as Objet
		case 'personne'		: from models.personne 		import Personne		as Objet
		case 'probleme'		: from models.probleme 		import Probleme		as Objet
		case 'savoir_faire'	: from models.savoir_faire	import SavoirFaire	as Objet
		case 'service' 		: from models.service 		import Service		as Objet
		case _ : return # TODO exception ou log

	DAO = Objet.DAO

	id		= Objet.format_id(id)							# formate l'id reçu
	machin	= DAO.get_from_id(id, exact=false, link=link)	# récupère à partir de l'id, erreur si innexistant

	if machin == None : return			# Si id innexistant
	assert isinstance(machin, Objet)

	match request.method :
		case 'GET' :
			pass
		case 'PUT' :

			# test et formattage des données envoyées par l'utilisateur
			data = Objet.format_data(request.form, edit=true)

			# Teste conflit de version évident. (sera re-testé dans une transaction en base au moment de l'enregistrement)
			if data.version != machin.version :
				print('conflit de version')
				return

			# Teste les modifications demandées sur l'objet
			machin, erreurs = machin.update(data)	# modification locales
			if erreurs :
				print('erreur édition locale')
				print(erreurs)
				return

			machin, erreurs = DAO.test_update(machin, data)	# modification globales
			if erreurs :
				print('erreurs globales')
				print(erreurs)
				return

			# enregistre les modifications (locales et globales) en base
			machin = DAO.svg(machin, data)

		case 'DELETE' :
			Objet.delete(id)


	return machin.linearise(link=link)


@app.route('/api/<Objet>', methods=['GET', 'POST'])
def api_personne(Objet):
	"""Actions sur les personnes
		GET		=> renvois la liste de tous les id existant en base
		POST 	=> ajoute une nouvelle personne en base
	"""

	# Récupération de la classe de l'objet
	match Objet :
		case 'besoin' 		: from models.besoin 		import Besoin		as Objet
		case 'contact' 		: from models.contact 		import Contact		as Objet
		case 'demande' 		: from models.demande 		import Demande		as Objet
		case 'identite' 	: from models.identite 		import Identite		as Objet
		case 'materiel' 	: from models.materiel 		import Materiel		as Objet
		case 'oeuvre' 		: from models.oeuvre 		import OEuvre		as Objet
		case 'personne' 	: from models.personne 		import Personne		as Objet
		case 'probleme' 	: from models.probleme 		import Probleme		as Objet
		case 'savoir_faire'	: from models.savoir_faire	import SavoirFaire	as Objet
		case 'service' 		: from models.service 		import Service		as Objet
		case _ : return # TODO exception ou log

	DAO = Objet.DAO
	match request.method :
		case 'POST':
			data = Objet.format_data(request.form, edit=false)	# teste et formate les données envoyées par l'utilisateur
			machin = DAO.add_new(data)
			return machin.linearise()

		case 'GET':
			return DAO.get_all_id()
