
CSS / Classe
------------

### <body>

.is-preload : Stops initial animations until page loads.

### <section> / <article>
.special : centre le texte

### #header / #banner / #footer / #navPanel, #titleBar

- #header li.active
- #navPanel, #titleBar : display en petit format
- #navPanel .link .indent-1 à 5


### <form>
.cta : largeur fixe

### <ul>

- .alt
- .icons    : liste d'icones
- .actions  : liste de boutons
  - .actions.special : écriture justifié
- .stacked  : flex column
- .fit      : largeur maximum

### .spotlight

- .style1 : bordure rouge
- .style2 : bordure bleue
- .style3 : bordure verte
- None    : bordure grise

- .top
- .bottom
- .left
- .right

- .inactive : désactive

### .wrapper

- .style2         : fond rouge
- .style1 / None  : fond noir

- .active :
- .primary

- fade-down
- fade-up
- fade


### .dropotron

- .active :
- .level-0


### .container

- .xsmall : largeur 25%
- .small  : largeur 50%
- .medium : largeur 75%
- .max    : largeur 100%
- .large  : largeur 125%
- .xlarge : largeur 150%

### .row

### .box
arrondi les angles
- .alt : sans arrondi

### .icon

- .solid :
- .brands : marques (police fontawesome)
- .alt
- .major
- .button : bouton

### .image

- .left / .right : fait flotter à gauche ou à droite
- .fit : étend sur toute la largeur

### .button

- .icon     : bouton icone
- .fit      : largeur max
- .small / .large : taille police plus grosse
- .primary  : change la palette
- .disabled : bouton désactivé

### .goto-next


Style alternatif
----------------

### .major

Affecte :
- <hr />    : plus gros
- <header>  : les marges ?
- <footer>  : les marges ?
- .icon     : change l'aspect


### .alt

Affecte
- .box
- <ul>
- <table>

### .align-left, .align-center, .align-right

Permet d'aligner le texte
