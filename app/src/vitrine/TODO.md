Ajouter des bundles pour le JS et le CSS
Ajouter un filtre pour transformer le scss en css, et virer les scripts .css
Ajouter un filtre pour minimifier le js
Ajouter un sous répertoire lib/ dans js pour les librairies jquery etc...

Modifier les urls des images, pour utiliser les images dans static (donc dans assets)
-> Voir si on ne peut pas créer un deuxième répertoire static avec une autre url racine (images)
=> voir si on ne peut pas généraliser ou fusionner le répertoire static webfonts
=> voir si on ne peut pas généraliser ou fusionner le répertoire static js/lib (notamment pour jquery)
=> voir si on ne peut pas fusionner le répertoire images des différents modules. L'idée est de séparer le style du contenu, et de ne pas giter les images mais seulement le code.
