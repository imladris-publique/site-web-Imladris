#! /bin/python3.10
#-*- coding: utf-8 -*-

from flask import Blueprint, current_app, render_template, request, send_file, abort, redirect, url_for
from pathlib import Path
from .template_utils import utils

# module pour le site vitrine.
#   templates dans vitrine.templates.vitrine
#   fichiers statiques : vitrine.static.{repo} -> /assets/{repo}/
module = Blueprint('vitrine', __name__,
    template_folder='templates',
    static_folder='static', static_url_path='assets'
    )

# sous module pour les fichiers d'images statiques : vitrine.images -> /images/
module_images = Blueprint('vitrine_img', __name__,
    template_folder=None,
    static_folder='images', static_url_path='images'
    )
module.register_blueprint(module_images)

# Importe les fonctions de templates dans le module
module.context_processor(lambda : utils) # prend en paramètre une fonction, qui ici renvoit uniquement utils

# ----------------------- Routes -------------------- #

# Test #

module.compteur = 1

@module.route('/test', methods=['GET'])
def test():
    """
    root page view that returns the index page and its data
    :return: index template
    """
    module.compteur += 1
    # season_anime = get_season_anime()
    return render_template('vitrine/home.html', my_list=['toto','titi','tutu', module.compteur])

# Index #

@module.route('/', methods=['GET'])
@module.route('/index.html', methods=['GET'])
def index():
    """
    Pages d'accueil
    """
    return render_template('vitrine/index.html')

# Qui #

@module.route('/qui.html', methods=['GET'])
def qui():
    """
    Qui sommes nous
    """
    return render_template('vitrine/qui.html')
# Quoi #

@module.route('/quoi.html', methods=['GET'])
def quoi():
    """
    Que faisons nous
    """
    return render_template('vitrine/quoi.html')

# Où #

@module.route('/ou.html', methods=['GET'])
def où():
    """
    Où nous trouver
    """
    return render_template('vitrine/où.html')

# Pourquoi #

@module.route('/pourquoi.html', methods=['GET'])
def pourquoi():
    """
    Pourquoi nous sommes nous créés
    """
    return render_template('vitrine/pourquoi.html')
    
# Quand #

@module.route('/quand.html', methods=['GET'])
def quand():
    """
    Historique et calendrier
    """
    return render_template('vitrine/quand.html')
    
# Comment #

@module.route('/comment.html', methods=['GET'])
def comment():
    """
    Nos méthodes et particularités
    """
    return render_template('vitrine/comment.html')

# Samain #

@module.route('/quand/samain.html', methods=['GET'])
@module.route('/samain.html', methods=['GET'])
def samain():
    """
    Pages du défilé aux lampions de Samain
    """
    return render_template('vitrine/quand/samain.html')

@module.route('/quand/samain/affiche.pdf', methods=['GET'])
@module.route('/samain/affiche.pdf', methods=['GET'])
def download():
    """
    Pour télécharger des fichiers
    """
    #TODO améliorer pour télécharger les fichiers dans Download (mais attention à la sécurité, ne télécharger que ces fichiers)
    return send_file('vitrine/download/Imladris-Samain-2022.pdf', as_attachment=True)
    #return send_file('vitrine/images/download/Imladris-Samain-2022.pdf', as_attachment=True)

# Beltaine #

@module.route('/quand/beltaine.html', methods=['GET'])
@module.route('/beltaine.html', methods=['GET'])
def beltaine():
    """
    Pages du livre collectif de Beltaine
    """
    return render_template('vitrine/quand/beltaine.html')

# Financement #

@module.route('/comment/financement.html', methods=['GET'])
@module.route('/financement.html', methods=['GET'])
def financement():
    """
    Pages expliquant notre business model
    """
    return render_template('vitrine/comment/financement.html')

# Licences #

@module.route('/comment/license', methods=['GET'])
@module.route('/comment/licenses', methods=['GET'])
@module.route('/comment/licences', methods=['GET'])
@module.route('/license', methods=['GET'])
@module.route('/licenses', methods=['GET'])
@module.route('/licences', methods=['GET'])
def licence_general_redirect():
    """Redirection anglais/français """
    return redirect(url_for('vitrine.licence_general'), code=302)

@module.route('/comment/licence', methods=['GET'])
@module.route('/licence', methods=['GET'])
def licence_general():
    """Page général des licences du site"""
    return render_template('vitrine/comment/licences.html')

@module.route('/comment/license/<id>', methods=['GET'])
@module.route('/comment/licenses/<id>', methods=['GET'])
@module.route('/comment/licences/<id>', methods=['GET'])
@module.route('/license/<id>', methods=['GET'])
@module.route('/licenses/<id>', methods=['GET'])
@module.route('/licences/<id>', methods=['GET'])
def licence_redirect(id):
    """Redirection anglais/français """
    return redirect(url_for('vitrine.licence', id=id), code=302)

@module.route('/comment/licence/<id>', methods=['GET'])
@module.route('/licence/<id>', methods=['GET'])
def licence(id):
    """
    Pages de licence d'une production
    """
    if len(id) != 8 :
        current_app.logger.info(f'id trop long {id}')
        abort(404)
    try :
        int(id,16)
    except ValueError :
        current_app.logger.warning(f'id non hexa {id}')
        abort(404)
    id = id.upper() # Pour le rendre indépendant de la casse
    return render_template(f'vitrine/comment/licences/{id}.html')

# Autres #

@module.route('/left-sidebar.html', methods=['GET'])
def left_sidebar():
    """
    page left_sidebar
    """
    return render_template('vitrine/left-sidebar.html')

@module.route('/right-sidebar.html', methods=['GET'])
def right_sidebar():
    """
    page right_sidebar
    """
    return render_template('vitrine/right-sidebar.html')

@module.route('/no-sidebar.html', methods=['GET'])
def no_sidebar():
    """
    page right_sidebar
    """
    return render_template('vitrine/no-sidebar.html')

@module.route('/elements.html', methods=['GET'])
def elements():
    """
    page elements
    """
    return render_template('vitrine/elements.html')

# En construction #

@module.route('/en-construction.html', methods=['GET'])
def en_construction():
    """
    Pages en construction
    """
    return render_template('vitrine/en-construction.html')
