
### Convocation

- Abstrait
  - le titre
  - la date de tenue
  - le lieu
  - l’ordre du jour : [ points ]
  - l’identifiant de la personne à qui est adressée la convocation, figurant dans le registre de son inscription
  - le cas échéant les modalités pour participer à distance ou effectuer une procuration
  - la signature cryptographique de la convocation par l’identité officielle de *La Coopérative* pour les convocations, appelée également *identifiant de convocation ;*
  - Annexes : [ ( document, signature ) ]  
  Documents nécessaires à la compréhension des dossiers traités, au titre de la réunion. Chacun des documents annexés sera signé par la signature cryptographique de l’identité officielle de *La Coopérative* pour les convocations.

#### Convocation d'AG

- le type de réunion ou d’*Assemblée Générale* et le cas échéant l’organisme réuni ;

#### Convocation du Conseil de Surveillance

- le motif de la convocation ;
- l’organisme ou le poste ayant pris la décision de réunir le *Conseil de Surveillance ;*
- le niveau d’alerte ;
- les indicateurs ayant nécessité la réunion du *Conseil de Surveillance.*


### Document

Documents nécessaires à la compréhension des dossiers traités au titre des réunions. Annexés aux convocation.

Cette classe ne s'occupe que des documents eux même, et peut être de leur versionning.
La partie signature est dépendante de la réunion.


### Réunion

#### Type

Chaque type de réunion peut être soit cyclique, soit ponctuelle.
- **cyclique**  
  Elles ont pour trait commun d’être réunies à périodes fixes, de pouvoir être prévues longtemps en avance et d’induire des rythmes rituels dans la vie coopérative.
- **ponctuelle**  
  Elles ont pour trait commun d’être réunies de façon évènementielle, potentiellement dans l’urgence ou pour des questions d’importance, introduisant des coupures dans le rythme habituel de la vie coopérative.

- Assemblée Générale
  - **ordinaire**  
    Ont trait aux réflexions et décisions opérationnelles, qui concernent le quotidien des interprétations et prises de décisions de chacun, dans le cadre du règlement intérieur et des statuts. (+ autres selon statuts)
    - *mensuelle* (cycliques)
    - *saisonnière* (cycliques)
      trimestrielle
    - *annuelle* (cycliques)
    - *réunie extraordinairement* (ponctuelle)
  - **extraordinaire** (ponctuelle)
    Ont trait aux décisions non prévues dans les statuts ; à la réflexion ou à la création, modification ou suppression de règles dans le règlement intérieur ; à la ratification de jurisprudences, les transformant ainsi en règles du règlement intérieur (+ autres selon statuts)
  - **de révision coopérative**  
    Ont trait au cas explicite de la révision coopérative.
    - cyclique
    - *réunie extraordinairement* (ponctuelle)
  - **constituante**  
    Ont trait à la réflexion, ou à la création, modification ou suppression de règles statutaires. (+ autres selon statuts)
    - *annuelle* (cyclique)
    - *réunie extraordinairement* (ponctuelle)
- Conseil de Surveillance
- Autre
