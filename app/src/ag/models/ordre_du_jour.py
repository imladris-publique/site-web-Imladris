from dataclasses import dataclass


@dataclass
class OdreDujour:
    """
    Liste ordonnée de sujet, affectée à une décision collective (ensemble de réunion).
    """
    #Propriétés propres
    liste: list[Sujet] # Liste ordonnée
    # Liens n-n Sujet


@dataclass
class Sujet:
    pass


@dataclass
class Document
    pass
