from enum import Enum
from dataclasses import dataclass
from datetime import datetime, timedelta

doc = """
Package du module AG, concernant l'objet métier Réunion.

Classes :
- TypeRéunion (Enum)
    - TypeConseilSurveillance
    - TypeAG
        - TypeAGOrdinaire
        - TypeAGExtraordinaire
        - TypeAGRévisionCoopérative
        - TypeAGConstituante

- Réunion

"""

class TypeRéunion(Enum):
    """
    Type de réunion.
    Énumération avec deux propriétés :
    - Un nom
    - un état booléen cyclique ou ponctuelle.
    """

    def __new__(cls, *args):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, nom: str, cyclique: bool):
        self._nom = str(nom)
        self._cyclique = bool(cyclique)

    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)

    def get_format(self)    -> str : return 'réunion {}'

    @property
    def nom(self) -> str :
        return self.get_format().format(self._nom)

    @property
    def sigle(self) -> str :
        return self.get_sigle().format(self._nom)

    @property
    def cyclique(self) -> bool :
        return self._cyclique

    @property
    def ponctuelle(self) -> bool :
        return not self._cyclique

class TypeEquipe(TypeRéunion):
    def get_format(self) -> str : return "Réunion d'équipe {}"

    RETRO           = ('rétrospective', True)
    PLAN_TACTIQUE   = ('plan tactique', True)
    STRATÉGIQUE     = ('plan stratégique mensuel', True)
    SAISON          = ('stratégique trimestiel', True)
    ANNUEL          = ('stratégique annuel', True)


class TypeConseilScT(TypeRéunion):
    def get_format(self) -> str : return "Réunion du conseil scientifique et technique {}"
    ORDINAIRE   = ('ordinaire', False)

class TypeConseilSurveillance(TypeRéunion):
    def get_format(self) -> str : return 'Réunion du conseil de surveillance {}'

    ORDINAIRE   = ('ordinaire', False)
    URGENCE     = ("d'urgence", False)

class TypeAG(TypeRéunion):
    def get_format(self) -> str : return 'assemblée générale {}'

class TypeAGOrdinaire(TypeAG):
    def get_format(self) -> str : return 'assemblée générale ordinaire {}'

    MENSUELLE   = ('mensuelle',     True)
    SAISON      = ('saisonnière',   True)
    ANNUELLE    = ('annuelle',      True)

    EXTRA       = ('réunie extraordinairement', False)


class TypeAGExtraordinaire(TypeAG):
    EXTRA       = ('extraordinaire', False)

class TypeAGRévisionCoopérative(TypeAG):
    def get_format(self) -> str : return 'assemblée générale de révision coopérative {}'

    PERIODIQUE  = ('périodique', True)
    EXTRA       = ('réunie extraordinairement', False)


class TypeAGConstituante(TypeAG):
    def get_format(self) -> str : return 'assemblée générale constituante {}'

    ANNUELLE    = ('annuelle', True)
    EXTRA       = ('réunie extraordinairement', False)


@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=True)
class DécisionCollective:
    """
    Abstract
    Modèle pour une décision collective.
    Processus de réflexion collective, pouvant mener à des prises de décision, concernant une ou plusieurs réunions du même type et partageant un même ordre du jour.
    Par exemple, une Assemblée Générale Ordinaire Mensuelle avec un ordre du jour bien déterminé concerne plusieurs réunions (au moins une par collège de vote actif)
    Le processus pouvant par ailleur s'effectuer en plusieurs étapes et nécessiter plusieurs réunions pour chacun des collèges.

    L'objet métier DécisionCollective concerne le rassemblement de l'ensemble de ces différentes réunions et leurs agencement.
    De façon à faciliter l'organisation, cet objet métier intègre plusieurs patern de conception :
    - Le patern Repository ( en tant que groupe de Réunion )
    - Le patern Composite ( permettant une arborescence d'autres décisions collectives )
    - Plusieurs patern Fabrique ( permettant la génération des différentes Réunions rassemblée et Convocations les concernants )

    Concrètement, l'objet DécisionCollective est abstrait et possède deux implémentations possibles :
    - DécisionCollectiveRacine :
        Possédent la référence du type et de l'ordre du jour. N'est lié à aucun parent (à lui même)
        Ne possède directement aucune Réunion, mais regroupe celle de ses DécisionCollectives enfant.
        Idem pour ses Collèges de membres.
    - DécisionCollectiveComposite :
        Ces instances permettent d'organiser les Réunions selon certaines méthodes,
        Possèdent nécessairement un parent DécisionCollective dont ils tirent les propriétés de type et l'ordre du jour.
        Possèdent un Collège ou le calcul à partir de ses DécisionCollective enfants.

    """



@dataclass(init=True, repr=True, eq=True, order=False, unsafe_hash=False, frozen=False, match_args=True, kw_only=False, slots=True)
class Réunion:
    """
    Modèle pour une réunion
    """

    _type: TypeRéunion
    _titre: str
    _date: datetime
    _duree: timedelta
    _lieu: str
    _ordre_du_jour: tuple

"""
TODO :
inspiration : Convocation.
- Abstrait
  - le titre
  - la date de tenue
  - le lieu
  - l’ordre du jour : [ points ]
  - l’identifiant de la personne à qui est adressée la convocation, figurant dans le registre de son inscription
  - le cas échéant les modalités pour participer à distance ou effectuer une procuration
  - la signature cryptographique de la convocation par l’identité officielle de *La Coopérative* pour les convocations, appelée également *identifiant de convocation ;*
  - Annexes : [ ( document, signature ) ]
  Documents nécessaires à la compréhension des dossiers traités, au titre de la réunion. Chacun des documents annexés sera signé par la signature cryptographique de l’identité officielle de *La Coopérative* pour les convocations.


En réalité, la convocation est une convocation à une réunion. Il y a une convocation par personne pouvant participer à cette réunion.
L'objet réunion contient les éléments communs à toutes les convocations.

En particulier la liste des documents nécessaires à l'étude des dossiers.

De plus, les objets réunion peuvent également servir pour la préparation des réunions (avant les convocations)
On devrait donc penser et ajouter un état ( prévisionnel, fixé, en cours, passé... )

Attention, une AG peut s'effectuer en plusieurs réunions, (déjà une par collège normalement), voir en plusieurs étapes comportant chacune un lot de réunion.

L'objet réunion peut également servir pour indiquer à postériori tout ce qui s'est passé pendant la réunion.
En particulier :
- Les infos comme des enregistrements devant être diffusés aux autres collèges.
- Les postes (président de réunion ? assesseur... )
- Les présences, les votes effectués etc...
- L'heure de cloture de la réunion (heure réelle)
- Les points abordés / reporté...


Penser qu'au delà du type de réunion, une réunion peut être d'urgence, avec d'autres modes de convocations.

"""
