#! /bin/python3
#-*- coding: utf-8 -*-

doc="""
Code lançant l'application.
"""

from src import app, arguments

if __name__ == '__main__':
	if arguments.get('debug') :
		print("Lancement de l'application")
		print({
			'port': arguments.port,
			'redis':'{}:{}'.format(arguments.redis_host,arguments.redis_port),
			'debug':arguments.debug
		})
		app.run(host=arguments.host, port=arguments.port, debug=arguments.debug)
	else:
		from waitress import serve
		serve(app, host=arguments.host, port=arguments.port, threads=arguments.get('threads'))
