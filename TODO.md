# Liste des trucs à faire
=========================


## Avant commit :
Les modifs en cours sont sur deux choses :
- la grosse modification : l'utilisation des polices avec une url absolue inter-site. => Vérifier que ça marche
- de petites retouches sur la configurations serveur ou les requirements, à vérifier tout de même avant mise en prod.


## Cache & Redis
----------------

### Cache flask

- Utiliser l'extension cache de flask ( cf lien dans les liens utiles du site vitrine )
- Configurer un docker et une base Redis pour le cache.
- Spécifier les configs pour une éventuelle utilisation de Redis distincte de celle du cache.
- Configurer le cache pour une utilisation de plusieurs instance de cache, pour des domaines spécifiques, afin de pouvoir migrer facilement la mémorisation de type "cache" de différents secteurs. ( localisation des données / type des données conservées / cache local ou distribué ... )
- Écrire une documentation explicite sur la politique de gestion des données concernées par le cache. Tant sur le plan conceptuel (principes de gestions de ces données) que sur l'installation et la configuration.

### Cache client

- Écrire une documentation claire, compréhensible et pédagogique sur la politique de gestion des données en cache sur les ordi clients. Cette documentation doit inclure un volet sécurité avancée.
- Utiliser différentes techniques : cookie, base de données de navigateur...
- Séparer les différents types de gestion de données ( sensible / affichage ... )
- TOUJOURS donner le choix de la conservation ou non des données en local.

## Gestion des données
----------------------

Mettre en place et créer une documentation (facilement modifiable) claire et pédagogique sur les politiques de gestion des données.

Présenter en particulier la politique de localisation des données :
- Client
- chez nous.
  - sur nos serveurs connectés, et où ils se situent.
  - sur des ordinateurs / disques déconnectés, et où ils se situent.

Présenter la séparation des types de données. ( données sensibles / personnelles ou non / anonymisées ... )

Présenter les politiques de sécurisation de ces données. En incluant un guide pédagogique pour assurer sois-même la sécurité de ses propres données.


Présenter les différents types de stockages, notamment sur la durée de conservation.
Attention, la différence entre un stockage de type cache et une base de données n'est qu'un détail. Les informations les plus importantes sont de savoir si elles sont conservés sur l'ordi client ou chez nous, si elles sont chiffrées ou non et par quel clef. Savoir si c'est sous forme de fichier, de base de donnée, de logs etc... c'est très secondaire, même si la politique de gestion de ces données diffèrent.


### Navigation connectée anonyme :

Proposer une option pour la navigation anonyme "connectée", c'est à dire avec des données personnelles exclusivement conservées sur l'ordinateur client.

Le serveur ne sert plus qu'à exposer les vues et l'ergonomie, et l'ensemble des données réelles sont utilisées dans ces modèles via javascript.

Penser à la sauvegarde dans un fichier local de ces données. Voir si on peut créer un code javascript qui crée un fichier sans avoir à passer par le serveur, et capable de le re-charger directement sans passer par le serveur (orientation vers une base de donnée locale, visible, sous forme d'un fichier de sauvegarde local).

Penser au chiffrement de ces données, en particulier celle des fichiers de sauvegarde de ces données.

Proposer un service de sauvegarde distinct de nos autres services, pour l'hébergement sur nos serveur de ces fichiers chiffrés. Y inclure le cas échéant un deuxième système de chiffrement lié au compte, pour s'assurer que les données de ecs fichiers sont chiffrées correctement (et pas avec des mots de passes trop simples).



## Branche git / répertoire git et architecture de développement.
-----------------------------------------------------------------


### Organiser le développement du code.

Clarifier l'organisation des répertoires git en ce qui concerne les sites et modules de la plateforme.

- Ce site est en main. -> clarifier le main / prod des branches de développements.
- vérifier la dépendance vis à vis du répertoire git "site vitrine"
- vérifier cette histoire de dépendance de branche à branche entre les répertoires git différents. -> automatiser le pull uniquement sur des branches locales "à merger"

Penser à des dépendances de type sous-répertoires git ou inclusion de répertoires indépendants.

Les modules sont actuellement pensées pour être tous inclus dans le même dossier racine. Utiliser un répertoire git central pour la partie générale et des sous-répertoires git pour les modules semblerait intéressant.
Vérifier s'il est possible de faciliter le clonage directe d'un seul module ( qui incluerai donc le répertoire général et le module spécifié )


### Documentation

Créer une documentation sur cette architecture. Attention, elle doit faciliter la contribution libre, et être suffisamment pédagogique pour servir d'exemple à des projets semblables.


## Base RDF
-----------

Trouver comment faire démarer et utiliser le docker de la base RDF :


notamment le volume docker : ``$PWD/data/RWStore.properties:/RWStore.properties``

et cette histoire de version 2.5.1 et de dockerfile.


 https://github.com/lyrasis/docker-blazegraph/blob/master/2.1.5/Dockerfile



Extension Flask ?
-----------------

https://pypi.org/search/?q=Flask+pdo&o=&c=Framework+%3A%3A+Flask

Pas forcément nécessaire.
( ex : on se sert du module redis en python pour se servir de la base redis, pas d'une extension de Flask )
