# Site Web Imladris

Voici le site web d'Imladris. Il s'agit d'un docker-compose avec un serveur flask, une base postgres et une base RDF.



## Déploiement

### Fichiers non gités
Plusieurs répertoires ne sont pas gités, et nécessitent un transfert de fichier sur le serveur.

- `/secrets/`  
    Contient les secrets docker
- `/app/src/vitrine/images/`  
    Contient les images du site
- `/app/src/vitrine/static/webfonts/`  
    Contient les polices utilisées sur le site
- `/app/src/vitrine/download`  
    Contient les fichiers à télécharger

** Ne jamais recopier les secrets docker locaux. En créer de nouveaux sur le serveur, adapté à un niveau "prod" **

### Script de synchronisation

Pour simplifier le travail de synchronisation des fichiers non gités, un script assez simple de synchronisation `rsync.bash` est utilisable, si vous êtes capable de comprendre le code.  
( **Ne jamais exécuter un code bash sur votre ordi perso sans l'avoir vérifié et tout compris** )

Il nécessite des variables d'environnement pour fonctionner (SSH_CLEF, SSH_PORT et DESTINATION)

Concrètement, il utilise rsync avec une connexion ssh avec clef ssh et port spécifique.  
La synchronisation ne pouvant se faire (de façon sécurisée) avec les droits sudo, il synchronise dans un premier temps sur notre répertoire perso sur le serveur.

Et dans un deuxième temps, le script `deploy.bash` doit être lancé sur le serveur pour déployer.
Concrètement, il recopie les fichiers synchronisé de votre répertoire perso dans le répertoire de prod. Puis il récupère la dernière version du repo git, puis reconstruit le docker, l'arrête et le relance.
( une coupure de quelques secondes est possible )

usage :
```
SSH_CLEF=<chemin de votre clef ssh> SSH_PORT=<port de connexion> DESTINATION=<utilisateur>@<serveur>:~/<chemin repo perso>/ ./rsync.bash
```
Si votre clef ssh est chiffré (recommandé), le script vous la demandera (c'est le prompt de rsync)

Une fois connecté sur votre serveur de prod, dans le répertoire où vous avez synchronisé les fichiers, vous pouvez lancer le script de déploiement.

Usage 1 :
```
DESTINATION=<chemin du repo git du serveur> ./deploy.bash
```

Usage 2 (création d'un fichier de variable d'environnement + usage de sudo pour le déploiement) :
```
echo 'DESTINATION="<chemin du repo git du serveur>"' > sync.env
sudo ./deploy.bash
```


### Variables d'environnement

Les secrets docker sont à créer.
Toutes les variables d'environnements utilisées par le site peuvent automatiquement être suffixés par _FILE afin d'être chargée par le biais d'un secret docker.
( pour un aperçu exaustif des variables d'environnements utilisées par l'application : cf `python/src/environnement.py` )

### docker-compose.override.yml
Vous pouvez surcharger le docker-compose en créant un fichier `docker-compose.override.yml`.
Ceci permet d'adapter votre déploiement sur votre serveur, sans modifier le fichier `docker-compose.yml` qui est lui gité.
Ceci simplifiera la mise à jour de votre application en effectuant simplement un git pull des dernières modifications.
Ceci permettra également d'être plus sécurisé car le fichier `docker-compose.override.yml` ne sera jamais gité (donc vos configurations ne seront pas publiés)

Il est notamment intéressant de configurer ses variables d'environnement dans ce fichier. (même les secrets docker, afin de ne pas même giter les chemins où se trouverons ces secrets dans votre conténaire)
C'est aussi dans ce fichier que nous choisissons les réseaux docker sur lesquels serons connectés les dockers de nos applications.
Vous pouvez également utiliser ce fichier pour ouvrir et effectuer les redirections de ports que vous souhaitez utiliser sur votre server pour cette application.


## Liens utiles :

- blueprint :
  - https://realpython.com/flask-blueprint/#how-to-use-flask-blueprints-to-improve-code-reuse
  - https://exploreflask.com/en/latest/blueprints.html
  - https://medium.com/innovation-res/flask-blueprints-tutorial-74862e5a19fd
  - En particulier pour les Bundle d'assets :  
    http://hackersandslackers.io/flask-blueprints/
  - Config sérieuse de Flask :  
    https://flask.palletsprojects.com/en/2.1.x/config/
- assets
  - https://flask-assets.readthedocs.io/en/latest/
  - https://webassets.readthedocs.io/en/latest/bundles.html#bundles
  - https://webassets.readthedocs.io/en/latest/css_compilers.html
- cache
  - https://flask-caching.readthedocs.io/en/latest/index.html

- jinja :
  - https://jinja.palletsprojects.com/en/3.1.x/templates/
  - https://jinja.palletsprojects.com/en/3.0.x/templates/#macros
- BDD (à plat, SQL, sqlalchemy ... )
  - https://www.membershipsthatpay.com/gestion-des-donnees-avec-python-sqlite-et-sqlalchemy-real-python/
- Serveur de prod waitress
  - https://flask.palletsprojects.com/en/2.1.x/tutorial/deploy/
  - https://docs.pylonsproject.org/projects/waitress/en/stable/arguments.html
- python
  - switch - case en python avec match :  
    https://benhoyt.com/writings/python-pattern-matching/
