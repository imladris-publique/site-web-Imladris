#!/bin/bash
# Script de déploiement sur le serveur (avec les droits sudo)

if [ -z "$DESTINATION" ]
then
  source sync.env
  if [ -z "$DESTINATION" ]
  then
    echo "Variable $DESTINATION non définie"
    exit 1
  fi
fi

mkdir -p  $DESTINATION/app/src/vitrine/download \
          $DESTINATION/app/src/vitrine/images \
          $DESTINATION/app/src/vitrine/static/webfonts

rsync -acv download/ $DESTINATION/app/src/vitrine/download/
rsync -acv images/ $DESTINATION/app/src/vitrine/images/
rsync -acv webfonts/ $DESTINATION/app/src/vitrine/static/webfonts/

cd $DESTINATION
git pull && docker-compose build && docker-compose down && docker-compose up -d
